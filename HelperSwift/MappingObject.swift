//
//  MappingObject.swift
//  HelperSwift
//
//  Created by Oknesta Developer on 10/3/17.
//  Copyright © 2017 Oknesta Developer. All rights reserved.
//

import Foundation


import Foundation
import ObjectMapper

class SKMessage : Mappable {
  
  var bodyMessage:String?
  var idMessage:Int?
  var userID:Int?
  var createdTime:String?
  
  
  // MARK: Mapping
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    bodyMessage   <- map["body"]
    idMessage     <- map["id"]
    userID        <- map["user_id"]
    createdTime   <- map["created_at"]
  }
  
}
