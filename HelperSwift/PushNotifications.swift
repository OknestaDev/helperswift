//
//  PushNotifications.swift
//  HelperSwift
//
//  Created by Oknesta Developer on 10/26/17.
//  Copyright © 2017 Oknesta Developer. All rights reserved.
//

import Foundation
import UserNotifications
import UIKit

//registerForPushNotifications() set in App Delegate, download 

/*
 IDs & Profiles -> Identifiers -> App IDs и выберите идентификатор приложения (App ID) для вашего приложения. В разделе Application Service, Push-уведомления должны отображаться как Configurable (Настраиваемые): 
 
 В Development SSL Certificate нажмите Create Certificate… и затем выполните шаги для создания CSR (Certificate Signing Request). После того, как у вас уже есть CSR, нажмите continue и следуйте инструкциям для создания (Generate) сертификата с помощью CSR. В завершении загрузите сертификат и кликните на него два раза, в итоге это должно добавить его в цепочку ключей (Keychain), в паре с приватным ключом:
 
 
  билд на устройстве
 
 */

extension AppDelegate {
  
  func registerForPushNotifications() {
    UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
      (granted, error) in
      print("Permission granted: \(granted)")
      guard granted else { return }
      self.getNotificationSettings()
    }
  }
  
  func getNotificationSettings() {
    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
      print("Notification settings: \(settings)")
      guard settings.authorizationStatus == .authorized else { return }
      UIApplication.shared.registerForRemoteNotifications()
    }
  }
  
  func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    let tokenParts = deviceToken.map { data -> String in
      return String(format: "%02.2hhx", data)
    }
    let token = tokenParts.joined()
    print("Device Token: \(token)")
  }
  
  
  func application(_ application: UIApplication,
                   didFailToRegisterForRemoteNotificationsWithError error: Error) {
    print("Failed to register: \(error)")
  }
  
  
}
