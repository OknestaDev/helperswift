//
//  FacebookLogin.swift
//  HelperSwift
//
//  Created by Oknesta Developer on 10/3/17.
//  Copyright © 2017 Oknesta Developer. All rights reserved.
//

import Foundation


func signInWithFacebook() {
  let FBLoginManager: FBSDKLoginManager = FBSDKLoginManager.init()
  FBLoginManager.logIn(withReadPermissions: ["public_profile","email"], from: self) { (result, error) in
    guard let result = result else {
      print("Login via Facebook failed")
      self.showAlert(title: "Error", message: "Login via Facebook failed.")
      return
    }
    
    if result.isCancelled {
      print("Login via Facebook Cancelled")
      return
    }
    
    guard error == nil else {
      print("Login via Facebook failed. Error: \(String(describing: error))")
      self.showAlert(title: "Error", message: "Login via Facebook failed.")
      return
    }
    guard let accessToken = FBSDKAccessToken.current().tokenString else {
      print("Login via Facebook failed. Error: access token is empty")
      self.showAlert(title: "Error", message: "Login via Facebook failed.")
      return
    }
    print("Facebook. AcccessToken: \(String(describing: accessToken))")
    
    self.registerViaSocial(accessToken: accessToken, provider: SocialProvider.facebook.rawValue, uid: nil, email: nil)
  }
}


func signInWithTwitter() {
  Twitter.sharedInstance().logIn(with: self) { (session, error) in
    guard error == nil else {
      print("Login via Twitter failed. Error: \(String(describing: error))")
      self.showAlert(title: "Error", message: "Login via Twitter failed.")
      return
    }
    guard let accessToken = session?.authToken, let uid = session?.userID else {
      print("Twitter session has empty accessToken or uid")
      self.showAlert(title: "Error", message: "Login via Twitter failed.")
      return
    }
    print("TWTRSession authToken. \(String(describing: accessToken))")
    
    let client = TWTRAPIClient.withCurrentUser()
    
    DispatchQueue.main.async {
      client.requestEmail { email, error in
        guard error == nil else {
          print("Login via Twitter failed. Error: \(String(describing: error))")
          self.showAlert(title: "Error", message: "Login via Twitter failed")
          return
        }
        guard let email = email else {
          print("error: email is empty");
          self.showAlert(title: "Error", message: "Login via Twitter failed")
          return
        }
        print("Signed in as \(String(describing: session?.userName))");
        
        self.registerViaSocial(accessToken: accessToken, provider: "twitter", uid: uid, email: email)
      }
    }
  }
}
