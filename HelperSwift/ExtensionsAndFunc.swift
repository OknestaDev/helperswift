//
//  ExtensionsAndFunc.swift
//  HelperSwift
//
//  Created by Oknesta Developer on 10/3/17.
//  Copyright © 2017 Oknesta Developer. All rights reserved.
//

import Foundation


extension UIColor {
  convenience init(hexString: String) {
    let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
    var int = UInt32()
    Scanner(string: hex).scanHexInt32(&int)
    let a, r, g, b: UInt32
    switch hex.characters.count {
    case 3: // RGB (12-bit)
      (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
    case 6: // RGB (24-bit)
      (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
    case 8: // ARGB (32-bit)
      (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
    default:
      (a, r, g, b) = (255, 0, 0, 0)
    }
    self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
  }
}

extension Array where Element: AnyObject {
  mutating func remove(object: Element) {
    if let index = index(where: { $0 === object }) {
      remove(at: index)
    }
  }
}


class func isValidEmail(_ email: String) -> Bool {
  let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
  let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
  return emailTest.evaluate(with: email)
}


class func is24or12TimeFormat() -> Bool {
  let dateString : String = DateFormatter.dateFormat(fromTemplate: "j", options: 0, locale: Locale.current)!
  if(dateString.contains("a")){
    return true // 12 h format
  }else{
    return false // 24 h format
  }
}

class func adapterForDateString(dateString: String) -> Date {
  let dateFormatter = DateFormatter()
  dateFormatter.dateFormat = correctFormat  //Your date format
  return dateFormatter.date(from: dateString)!
}
