//
//  ServerURLSession.swift
//  HelperSwift
//
//  Created by Oknesta Developer on 10/3/17.
//  Copyright © 2017 Oknesta Developer. All rights reserved.
//

import Foundation

extension NetworkManager {
  
  func getListAvailability (complete: @escaping (_ response:[SKEvent]?,_ error: String?)->()) {
    
    let path = "pathUrl"
    let tokenActive = "token"
    let uidActive "uid"
    let clientActive = "clientid"
    
    guard let URL = URL.init(string: path) else {
      print("Error: failed URL initalization ")
      return
    }
    
    guard let token = tokenActive else {
      print("Error: failed token ")
      return
    }
    guard let uid = uidActive else {
      print("Error: failed uid ")
      return
    }
    guard let client = clientActive else {
      print("Error: failed client ")
      return
    }
    
    var request = URLRequest(url: URL)
    let headers = [
      "access-token": token,
      "client": client,
      "uid": uid,
      ]
    
    request.httpMethod = "GET"
    request.allHTTPHeaderFields = headers
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
      DispatchQueue.main.async {
        do {
          guard let data = data, error == nil else {
            print("error=\(String(describing: error))")
            complete(nil,error?.localizedDescription)
            return
          }
          guard let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String:Any]] else {return}
          print(json)
          // use ObjectMapper
          let events = json.flatMap({ SKEvent(JSON: $0) })
          complete(events,nil)
          return
        } catch let error as NSError {
          print(error)
          complete(nil,error.localizedDescription)
        }
        
      }
      }.resume()
  }
  
}
